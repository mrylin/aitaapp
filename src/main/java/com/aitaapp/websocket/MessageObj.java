package com.aitaapp.websocket;

import lombok.Data;

@Data
public class MessageObj {
    private Long currentUser; //当前用户id
    private Long receive; //对方id
    private String msg; //消息
    private Long time; //发送时间
}
