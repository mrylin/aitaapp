package com.aitaapp.websocket;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Message;
import com.aitaapp.entity.MessageList;
import com.aitaapp.entity.User;
import com.aitaapp.mapper.MessageListMapper;
import com.aitaapp.mapper.MessageMapper;
import com.aitaapp.mapper.UserMapper;
import com.aitaapp.service.MessageListService;
import com.aitaapp.service.MessageService;
import com.aitaapp.utils.DateTime;
import com.aitaapp.utils.Json;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.websocket.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;


@Slf4j
@Service
public class SocketService {
    public MessageListMapper messageListMapper;
    public MessageMapper messageMapper;
    public MessageListService messageListService;
    public MessageService messageService;
    public UserMapper userMapper;

    @Autowired
    SocketService(MessageListMapper messageListMapper, MessageMapper messageMapper, MessageListService messageListService, MessageService messageService,UserMapper userMapper) {
        this.messageListMapper = messageListMapper;
        this.messageMapper = messageMapper;
        this.messageListService = messageListService;
        this.messageService = messageService;
        this.userMapper = userMapper;
    }

    public void message(String data, ConcurrentHashMap<String, Session> sessionPool, String userId) throws JsonProcessingException {
        //返回检测心跳
        if (Objects.equals(data, "check")) {
            Session sessionCheck = sessionPool.get(userId);
            if (sessionCheck != null && sessionCheck.isOpen()) {
                try {
                    //发送心跳
                    sessionCheck.getAsyncRemote().sendText("online");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        MessageObj messageObj = Json.obj(data);
        MessageList messageList = messageListMapper.getMessageList(messageObj.getCurrentUser(), messageObj.getReceive());
        MessageList messageListReceive = messageListMapper.getMessageList(messageObj.getReceive(), messageObj.getCurrentUser());
        //添加聊天列表
        if (messageList == null && messageListReceive == null) {
            //对方和自己都没查到就都添加
            messageListMapper.addMessageList(messageObj.getCurrentUser(), messageObj.getCurrentUser(), messageObj.getReceive());
            messageListMapper.addMessageList(messageObj.getCurrentUser(), messageObj.getReceive(), messageObj.getReceive());
        } else if (messageList == null) {
            //添加自己的
            messageListMapper.addMessageList(messageObj.getCurrentUser(), messageObj.getCurrentUser(), messageObj.getReceive());
        } else if (messageListReceive == null) {
            //添加对方
            messageListMapper.addMessageList(messageObj.getCurrentUser(), messageObj.getReceive(), messageObj.getReceive());
        }

        //新增聊天消息详情表
        Message msg = new Message();
        msg.setMessage(messageObj.getMsg());
        msg.setSend(messageObj.getCurrentUser());
        msg.setReceive(messageObj.getReceive());
        msg.setMsgTime(messageObj.getTime());
        msg.setExpirationTime(DateTime.getExpirationTime());
        msg.setReadMsg(0L);
        msg.setCurrentUser(messageObj.getCurrentUser());

        int num = messageMapper.addMessage(msg);
        if (num > 0) {
            msg.setCurrentUser(messageObj.getReceive());
            messageMapper.addMessage(msg);
        }

        Session session = sessionPool.get(String.valueOf(messageObj.getReceive()));
        if (session != null && session.isOpen()) {
            try {
                //查询聊天记录
                Message message = messageMapper.getById(msg.getId());
                User user = userMapper.getUserId(msg.getSend());
                message.setUrl(user.getHeadSculpture());
                ResultSet<Message> resultSet = new ResultSet<>();
                resultSet.setData(message);

                //在线就推送消息给对方
                session.getAsyncRemote().sendText(Json.string(resultSet));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
