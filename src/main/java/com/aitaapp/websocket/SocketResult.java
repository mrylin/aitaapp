package com.aitaapp.websocket;

import com.aitaapp.constant.ResponseCode;
import com.aitaapp.entity.Message;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;


@Data
@Component
public class SocketResult {
    private Message msgList;
    private List groupList;
}
