package com.aitaapp.websocket;

import com.aitaapp.bean.ResultSet;
import com.google.gson.Gson;
import com.qiniu.cdn.CdnManager;
import com.qiniu.cdn.CdnResult;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.model.BatchStatus;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QiNiuUpload {
    String accessKey = "CVPW2a_SvzMhLCn3JUIxDDNKAhVoTua8d3QIV7R9";
    String secretKey = "T2sH4fpcwO0dAXLlxiuaQsO5W8bCKK75k2Vzzvf5";
    String bucketName = "aitalove";
    String url = "http://s45z6wtbu.hd-bkt.clouddn.com/";

    //上传文件
    public String Upload(MultipartFile file) {
        //构造一个带指定Region对象的配置类
        Configuration cfg = new Configuration(Region.region0());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
        UploadManager uploadManager = new UploadManager(cfg);

        //凭证
        Auth auth = Auth.create(this.accessKey, this.secretKey);
        String upToken = auth.uploadToken(this.bucketName);
        DefaultPutRet putRet = null;
        try {
            //key:文件名不传默认使用上传成功hash
            Response response = uploadManager.put(file.getInputStream(), null, upToken, null, null);
            //解析上传成功的结果
            putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);

            System.out.println("上传成功! 生成的key是: " + putRet.key);
            System.out.println("上传成功! 生成的hash是: " + putRet.hash);

        } catch (IOException e) {

        }
        return this.url + putRet.hash;
    }

    //删除单个文件
    public int delFile(String fileName) {
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.autoRegion());

        Auth auth = Auth.create(this.accessKey, this.secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        Response response = null;
        try {
            response = bucketManager.delete(this.bucketName, fileName);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
        }
        if (response != null) {
            return response.statusCode;
        }
        QiNiuUpload qiNiuUpload = new QiNiuUpload();
        qiNiuUpload.flushedCdn();
        return 0;
    }

    //删除多个文件
    public ResultSet delBatchFile(List<String> list) {
        Configuration cfg = new Configuration(Region.autoRegion());
        Auth auth = Auth.create(this.accessKey, this.secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);

        try {
            String[] keyList = list.stream().toArray(String[]::new);
            BucketManager.BatchOperations batchOperations = new BucketManager.BatchOperations();
            batchOperations.addDeleteOp(this.bucketName, keyList);
            Response response = bucketManager.batch(batchOperations);
            BatchStatus[] batchStatusList = response.jsonToObject(BatchStatus[].class);
            for (int i = 0; i < keyList.length; i++) {
                BatchStatus status = batchStatusList[i];
                String key = keyList[i];
                System.out.print(key + "\t");
                if (status.code == 200) {
                    System.out.println("delete success");
                } else {
                    System.out.println(status.data.error);
                }
            }
        } catch (QiniuException ex) {
            System.err.println(ex.response.toString());
        }
        QiNiuUpload qiNiuUpload = new QiNiuUpload();
        qiNiuUpload.flushedCdn();
        return new ResultSet();
    }

    public void flushedCdn() {
        Auth auth = Auth.create(this.accessKey, this.secretKey);
        CdnManager c = new CdnManager(auth);
        String[] dirs = new String[]{this.url};
        try {
            CdnResult.RefreshResult result = c.refreshDirs(dirs);
            System.out.println(result.code + "刷新成功---------");
            //获取其他的回复内容
        } catch (QiniuException e) {
            System.err.println(e.response.toString());
        }
    }
}
