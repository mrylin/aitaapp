package com.aitaapp.service.impl;


import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Message;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface MessageImpl {
    ResultSet<List<Message>> getMsgDetailed(int pageIndex, int pageSize, Long send, Long receive);
    ResultSet<List<Message>> getAllMsg(Long id);
    ResultSet updateById(Long id);
}
