package com.aitaapp.service.impl;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Visitor;
import org.springframework.web.bind.annotation.RequestBody;

public interface VisitorImpl {
    ResultSet<String> addVisitor(Visitor visitor);
}
