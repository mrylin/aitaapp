package com.aitaapp.service.impl;


import com.aitaapp.bean.Like;
import com.aitaapp.bean.PaginationResult;
import com.aitaapp.bean.ResultSet;
import com.aitaapp.bean.Login;
import com.aitaapp.bean.request.ReqUserRecommend;
import com.aitaapp.entity.LikeFootprints;
import com.aitaapp.entity.User;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface UserImpl {
    Login login(Long phone, String code, HttpServletRequest request);

    PaginationResult<User> getRecommend(int pageIndex, int pageSize, Boolean latest, User user);

    PaginationResult<User> getSiftRecommend(int pageIndex, int pageSize, Boolean latest, ReqUserRecommend reqUserRecommend);

    ResultSet<Long> IlikeIt(LikeFootprints likeFootprints);

    ResultSet<User> getUserId(Long receive, Long id);

    ResultSet<Like> getCountLike(Long id);

    ResultSet<List<User>> getLikeUser(int pageIndex, int pageSize, Long id, Long type);

    ResultSet<User> getCurrentUser(Long id);

    ResultSet<Integer> updateUser(User user);

    ResultSet<Integer> updateUserTimeIp(User user, HttpServletRequest request);
}
