package com.aitaapp.service.impl;


import com.aitaapp.bean.ResultSet;
import com.aitaapp.bean.SystemMsg;
import com.aitaapp.entity.User;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface MessageListImpl {
    ResultSet<List<User>> getMsgList(Long id);

    ResultSet<SystemMsg> getSystemMsg(Long id);

    ResultSet delMsgList(Long send, Long receive);

    ResultSet updateMsgList(Long send, Long receive);

    ResultSet updateSystemMsg(Long receive);
}
