package com.aitaapp.service.impl;


import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Report;
import org.springframework.web.bind.annotation.RequestBody;

public interface ReportImpl {
    ResultSet<Number> report(Report report);
}
