package com.aitaapp.service;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Message;
import com.aitaapp.entity.User;
import com.aitaapp.mapper.MessageMapper;
import com.aitaapp.mapper.UserMapper;
import com.aitaapp.service.impl.MessageImpl;
import com.aitaapp.service.impl.UserImpl;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService implements MessageImpl {
    public MessageMapper messageMapper;
    public UserMapper userMapper;

    @Autowired
    public MessageService(MessageMapper messageMapper, UserMapper userMapper) {
        this.messageMapper = messageMapper;
        this.userMapper = userMapper;
    }

    @Override
    public ResultSet<List<Message>> getMsgDetailed(int pageIndex, int pageSize, Long send, Long receive) {
        PageHelper.startPage(pageIndex, pageSize);
        List<Message> messageList = messageMapper.getMsgDetailed(send, receive);
        messageMapper.UpdateRead(send, receive);
        User sendUser = null;
        User receiveUser = null;
        for (int i = 0; i <= 1; i++) {
            if (i == 0) {
                sendUser = userMapper.getUserId(send);
            } else {
                receiveUser = userMapper.getUserId(receive);
            }
        }
        User finalReceiveUser = receiveUser;
        User finalSendUser = sendUser;
        messageList.forEach(item -> {
            if (item.getSend().equals(send)) {
                item.setUrl(finalSendUser.getHeadSculpture());
            } else if (item.getSend().equals(receive)) {
                item.setUrl(finalReceiveUser.getHeadSculpture());
            }
        });

        ResultSet<List<Message>> resultSet = new ResultSet<>();
        resultSet.setData(messageList);
        return resultSet;
    }

    @Override
    public ResultSet<List<Message>> getAllMsg(Long id) {
        List<Message> messages = messageMapper.getAllMsg(id);
        ResultSet<List<Message>> resultSet = new ResultSet<>();
        resultSet.setData(messages);
        return resultSet;
    }

    @Override
    public ResultSet updateById(Long id) {
        messageMapper.updateById(id);
        ResultSet resultSet = new ResultSet();
        return resultSet;
    }
}
