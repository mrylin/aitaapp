package com.aitaapp.service;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Visitor;
import com.aitaapp.mapper.VisitorMapper;
import com.aitaapp.service.impl.VisitorImpl;
import com.aitaapp.utils.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisitorService implements VisitorImpl {
    public VisitorMapper visitorMapper;

    @Autowired
    public VisitorService(VisitorMapper visitorMapper) {
        this.visitorMapper = visitorMapper;
    }

    @Override
    public ResultSet<String> addVisitor(Visitor visitor) {
        Visitor visitor1 = visitorMapper.getVisitor(visitor.getPid(), visitor.getHaveYouSeenMe());
        ResultSet<String> resultSet = new ResultSet<>();
        if (visitor1 == null) {
            int num = visitorMapper.addVisitor(visitor.getPid(), visitor.getHaveYouSeenMe(), DateTime.getExpirationTime7());
            if (num > 0) {
                resultSet.setData("添加成功");
            }
        }
        return resultSet;
    }
}
