package com.aitaapp.service;

import com.aitaapp.mapper.LifePhotosMapper;
import com.aitaapp.mapper.UserMapper;
import com.aitaapp.service.impl.LifePhotosImpl;
import com.aitaapp.service.impl.UserImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LifePhotosService implements LifePhotosImpl {
    public LifePhotosMapper lifePhotosMapper;

    @Autowired
    public LifePhotosService(LifePhotosMapper lifePhotosMapper) {
        this.lifePhotosMapper = lifePhotosMapper;
    }
}
