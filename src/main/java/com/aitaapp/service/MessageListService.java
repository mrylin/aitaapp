package com.aitaapp.service;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.bean.SystemMsg;
import com.aitaapp.entity.Message;
import com.aitaapp.entity.MessageList;
import com.aitaapp.entity.User;
import com.aitaapp.mapper.MessageListMapper;
import com.aitaapp.mapper.MessageMapper;
import com.aitaapp.mapper.UserMapper;
import com.aitaapp.service.impl.MessageListImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageListService implements MessageListImpl {
    public MessageListMapper messageListMapper;

    public UserMapper userMapper;

    public MessageMapper messageMapper;

    @Autowired
    public MessageListService(MessageListMapper messageListMapper, UserMapper userMapper, MessageMapper messageMapper) {
        this.messageListMapper = messageListMapper;
        this.userMapper = userMapper;
        this.messageMapper = messageMapper;
    }

    @Override
    public ResultSet<List<User>> getMsgList(Long id) {
        //测试用60---86
        List<MessageList> messageLists = messageListMapper.getMsgList(id);
        ArrayList<Long> ids = new ArrayList<>();
        messageLists.forEach(item -> {
            if (Objects.equals(item.getSend(), id)) {
                //取出接收方的id
                ids.add(item.getReceive());
            } else if (Objects.equals(item.getReceive(), id)) {
                //取出发送方的id
                ids.add(item.getSend());
            }
        });
        List<User> user = null;
        if (!ids.isEmpty()) {
            //消息列表用户信息
            user = userMapper.getForEach(ids);
        }

        if (user != null) {
            user.forEach(listItem -> {
                Long count = messageMapper.getMsgCount(listItem.getId(), id, 0L);
                Message message = messageMapper.getNewMsgTime(listItem.getId(), id);
                listItem.setUnReadCount(count);
                listItem.setMsgTime(message.getMsgTime());
            });
        }
        ResultSet<List<User>> resultSet = new ResultSet<>();
        resultSet.setData(user);
        return resultSet;
    }

    @Override
    public ResultSet<SystemMsg> getSystemMsg(Long id) {
        Message message = messageMapper.getSystemMsg(id);
        ResultSet resultSet = new ResultSet();
        if (message!=null){
            Long count = messageMapper.getSystemMsgCount(id);
            SystemMsg systemMsg = new SystemMsg();
            systemMsg.setMessage(message.getMessage());
            systemMsg.setMsgTime(message.getMsgTime());
            systemMsg.setUnReadCount(count);
            resultSet.setData(systemMsg);
        }
        return resultSet;
    }

    @Override
    public ResultSet delMsgList(Long send, Long receive) {
        int num = messageListMapper.delMsgList(send, receive);
        if (num > 0) return new ResultSet<>();
        return null;
    }

    @Override
    public ResultSet updateMsgList(Long send, Long receive) {
        Integer num = messageMapper.UpdateRead(send, receive);
        if (num > 0) return new ResultSet<>();
        return null;
    }

    @Override
    public ResultSet updateSystemMsg(Long receive) {
        messageMapper.updateSystemMsg(receive);
        return new ResultSet<>();

    }
}
