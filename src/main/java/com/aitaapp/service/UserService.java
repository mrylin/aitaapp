package com.aitaapp.service;

import com.aitaapp.bean.Like;
import com.aitaapp.bean.Login;
import com.aitaapp.bean.PaginationResult;
import com.aitaapp.bean.ResultSet;
import com.aitaapp.bean.request.ReqUserRecommend;
import com.aitaapp.entity.LifePhotos;
import com.aitaapp.entity.LikeFootprints;
import com.aitaapp.entity.User;
import com.aitaapp.entity.Visitor;
import com.aitaapp.mapper.LifePhotosMapper;
import com.aitaapp.mapper.LikeFootprintsMapper;
import com.aitaapp.mapper.UserMapper;
import com.aitaapp.mapper.VisitorMapper;
import com.aitaapp.service.impl.UserImpl;
import com.aitaapp.utils.IpUtils;
import com.aitaapp.utils.DateTime;
import com.aitaapp.utils.RandomName;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class UserService implements UserImpl {
    public UserMapper userMapper;
    public LikeFootprintsMapper likeFootprintsMapper;
    public LifePhotosMapper lifePhotosMapper;
    public VisitorMapper visitorMapper;

    @Autowired
    public UserService(UserMapper useMapper, LikeFootprintsMapper likeFootprintsMapper, LifePhotosMapper lifePhotosMapper, VisitorMapper visitorMapper) {
        this.userMapper = useMapper;
        this.lifePhotosMapper = lifePhotosMapper;
        this.likeFootprintsMapper = likeFootprintsMapper;
        this.visitorMapper = visitorMapper;
    }

    @Override
    public Login login(Long phone, String code, HttpServletRequest request) {
        HttpSession session = request.getSession();
        User user = userMapper.login(phone);
        Login login = new Login();
        String sessionId = session.getId();
        if (user != null && Objects.equals(code, "1234")) {
            login.setToken(sessionId);
            session.setAttribute(String.valueOf(user.getMobilePhone()), sessionId);
        } else if (user == null) {
            String city = IpUtils.stringSplit(Objects.requireNonNull(IpUtils.getIp2region(IpUtils.getIp(request))))[1].replace("省", "");
            int num = userMapper.insertUser(DateTime.getTimestamp(),
                    phone, IpUtils.getIp(request),
                    city, RandomName.getRandomJianHan());
            if (num > 0) {
                user = userMapper.login(phone);
                login.setToken(sessionId);
                session.setAttribute(String.valueOf(phone), sessionId);
            }
        }
        if (user != null && user.getEndTime() != null) {
            Date timestamp1 = new Date(user.getEndTime());
            Date timestamp2 = new Date(DateTime.getTimestamp());
            // 方法1：使用compareTo()方法
            int result = timestamp1.compareTo(timestamp2);

            // 如果result < 0，则timestamp1小于timestamp2；
            // 如果result > 0，则timestamp1大于timestamp2；
            // 如果result = 0，则timestamp1等于timestamp2。

            //结束时间小于当前时间
            if (result < 0) {
                //清空封禁类型和结束时间
                user.setEndTime(null);
                user.setPunishmentType(null);
                userMapper.updatePunishmentType(null, null, user.getId());
            }
        }
        List<LifePhotos> list = lifePhotosMapper.getListPhotos(user.getId());
        user.setLifePhotos(list);
        login.setData(user);
        return login;
    }

    @Override
    public PaginationResult<User> getRecommend(int pageIndex, int pageSize, Boolean latest, User user) {
        if (Objects.equals(user.getGender(), "") || user.getGender() == null) {
            User genderUser = userMapper.getUserId(user.getId());
            if (genderUser != null && Objects.equals(genderUser.getGender(), "男")) {
                user.setGender("女");
            } else if (genderUser != null && Objects.equals(genderUser.getGender(), "女")) {
                user.setGender("男");
            }
        }
        //用id条件查询pid喜欢列表,查询到的我喜欢的id遍历修改用户like属性为1，
        if (latest) {
            PageHelper.startPage(pageIndex, pageSize, "creationTime DESC");
        } else {
            PageHelper.startPage(pageIndex, pageSize);
        }
        List<User> users = userMapper.getRecommend(user);

        if (user.getId() != null) {
            ArrayList<LikeFootprints> likeIt = likeFootprintsMapper.getLikeIt(user.getId());
            likeIt.forEach(item -> users.forEach(uItem -> {
                if (Objects.equals(item.getLikeIt(), uItem.getId())) {
                    uItem.setLike(1);
                }
            }));
        }
        users.forEach(item -> {
            item.setYear(LocalDate.now().getYear() - item.getAge());
            item.setMobilePhone(null);
        });
        PaginationResult<User> result = new PaginationResult<>();
        PageInfo<User> pageInfo = new PageInfo<>(users);
        result.setData(users);
        result.setPageIndex(pageInfo.getPageNum());
        result.setTotal(pageInfo.getTotal());
        return result;
    }

    @Override
    public PaginationResult<User> getSiftRecommend(int pageIndex, int pageSize, Boolean latest, ReqUserRecommend reqUserRecommend) {
        if (Objects.equals(reqUserRecommend.getGender(), "") || reqUserRecommend.getGender() == null) {
            User genderUser = userMapper.getUserId(reqUserRecommend.getId());
            if (genderUser != null && Objects.equals(genderUser.getGender(), "男")) {
                reqUserRecommend.setGender("女");
            } else if (genderUser != null && Objects.equals(genderUser.getGender(), "女")) {
                reqUserRecommend.setGender("男");
            }
        }

        PageHelper.startPage(pageIndex, pageSize);
        List<User> users = userMapper.getSiftRecommend(latest, reqUserRecommend);

        if (reqUserRecommend.getId() != null) {
            ArrayList<LikeFootprints> likeIt = likeFootprintsMapper.getLikeIt(reqUserRecommend.getId());
            likeIt.forEach(item -> users.forEach(uItem -> {
                if (Objects.equals(item.getLikeIt(), uItem.getId())) {
                    uItem.setLike(1);
                }
            }));
        }
        users.forEach(item -> {
            item.setYear(LocalDate.now().getYear() - item.getAge());
            item.setMobilePhone(null);
        });
        PaginationResult<User> result = new PaginationResult<>();
        PageInfo<User> pageInfo = new PageInfo<>(users);
        result.setData(users);
        result.setPageIndex(pageInfo.getPageNum());
        result.setTotal(pageInfo.getTotal());
        return result;
    }

    @Override
    public ResultSet<Long> IlikeIt(LikeFootprints likeFootprints) {
        LikeFootprints ilikeIt = likeFootprintsMapper.getIlikeIt(likeFootprints.getPid(), likeFootprints.getLikeIt());
        ResultSet<Long> resultSet = new ResultSet<>();
        Long num = 0L;
        if (ilikeIt == null) {
            //添加我喜欢
            num = likeFootprintsMapper.addIlikeIt(likeFootprints);
            //添加喜欢我的
            likeFootprintsMapper.addIikeMe(likeFootprints.getPid(), likeFootprints.getLikeIt());
        } else {
            likeFootprintsMapper.delLike(likeFootprints.getPid(), likeFootprints.getLikeIt());
            likeFootprintsMapper.delIikeMe(likeFootprints.getLikeIt(), likeFootprints.getPid());
        }
        if (num > 0) {
            resultSet.setData(num);
        }
        return resultSet;
    }

    @Override
    public ResultSet<User> getUserId(Long receive, Long id) {
        User resultSet = userMapper.getUserId(receive);
        List<LifePhotos> list = lifePhotosMapper.getListPhotos(receive);
        resultSet.setLifePhotos(list);
        LikeFootprints likeFootprints = likeFootprintsMapper.getUserLikeIt(receive, id);
        if (likeFootprints != null) {
            resultSet.setLike(1);
        }
        resultSet.setMobilePhone(null);
        ResultSet<User> userResultSet = new ResultSet<>();
        userResultSet.setData(resultSet);
        return userResultSet;
    }

    @Override
    public ResultSet<Like> getCountLike(Long id) {
        Like like = new Like();
        Long countILike = likeFootprintsMapper.countILike(id);
        Long countLikeMe = likeFootprintsMapper.countLikeMe(id);
        Long countSeenMe = visitorMapper.countSeenMe(id);
        like.setILike(countILike);
        like.setLikeMine(countLikeMe);
        like.setSeenMe(countSeenMe);
        ResultSet<Like> resultSet = new ResultSet<>();
        resultSet.setData(like);
        return resultSet;
    }

    @Override
    public ResultSet<List<User>> getLikeUser(int pageIndex, int pageSize, Long id, Long type) {
        ResultSet<List<User>> resultSet = new ResultSet<>();
        //type=1查我喜欢的。2查喜欢我的。3查我看过的
        if (type == 1) {
            PageHelper.startPage(pageIndex, pageSize);
            List<LikeFootprints> likeFootprints = likeFootprintsMapper.getILike(id);

            List<Long> list = new ArrayList<>();
            likeFootprints.forEach(item -> {
                list.add(item.getLikeIt());
            });
            if (!likeFootprints.isEmpty()) {
                List<User> user = userMapper.getForEach(list);
                user.forEach(item -> {
                    item.setLike(1);
                    item.setMobilePhone(null);
                    item.setYear(LocalDate.now().getYear() - item.getAge());
                });
                resultSet.setData(user);
            }
        } else if (type == 2) {
            PageHelper.startPage(pageIndex, pageSize);
            List<LikeFootprints> likeFootprints = likeFootprintsMapper.getLikeMe(id);

            List<Long> list = new ArrayList<>();
            likeFootprints.forEach(item -> {
                list.add(item.getLikeMe());
            });
            if (!likeFootprints.isEmpty()) {
                List<User> user = userMapper.getForEach(list);
                user.forEach(item -> {
                    item.setLike(1);
                    item.setMobilePhone(null);
                    item.setYear(LocalDate.now().getYear() - item.getAge());
                });
                resultSet.setData(user);
            }
        } else if (type == 3) {
            PageHelper.startPage(pageIndex, pageSize);
            List<Visitor> visitors = visitorMapper.getSeenMe(id);

            List<Long> list = new ArrayList<>();
            visitors.forEach(item -> {
                list.add(item.getHaveYouSeenMe());

            });
            if (!visitors.isEmpty()) {
                List<User> user = userMapper.getForEach(list);
                user.forEach(item -> {
                    item.setLike(1);
                    item.setMobilePhone(null);
                    item.setYear(LocalDate.now().getYear() - item.getAge());
                });
                resultSet.setData(user);
            }
        }



        return resultSet;
    }

    @Override
    public ResultSet<User> getCurrentUser(Long id) {
        ResultSet<User> resultSet = new ResultSet<>();
        User user = userMapper.getUserId(id);
        List<LifePhotos> list = lifePhotosMapper.getListPhotos(id);
        user.setLifePhotos(list);

        if (user.getEndTime() != null) {
            Date timestamp1 = new Date(user.getEndTime());
            Date timestamp2 = new Date(DateTime.getTimestamp());
            // 方法1：使用compareTo()方法
            int result = timestamp1.compareTo(timestamp2);

            // 如果result < 0，则timestamp1小于timestamp2；
            // 如果result > 0，则timestamp1大于timestamp2；
            // 如果result = 0，则timestamp1等于timestamp2。

            //结束时间小于当前时间
            if (result < 0) {
                //清空封禁类型和结束时间
                user.setEndTime(null);
                user.setPunishmentType(null);
                userMapper.updatePunishmentType(null, null, user.getId());
            }
        }
        resultSet.setData(user);
        return resultSet;
    }

    @Override
    public ResultSet<Integer> updateUser(User user) {
        ResultSet<Integer> resultSet = new ResultSet<>();
        int num = userMapper.updateUser(user);
        lifePhotosMapper.delPhotos(user.getId());
        user.getPhotos().forEach(item -> {
            lifePhotosMapper.addPhotos(item, user.getId());
        });
        resultSet.setData(num);
        return resultSet;
    }

    @Override
    public ResultSet<Integer> updateUserTimeIp(User user, HttpServletRequest request) {
        user.setLastActiveTime(DateTime.getTimestamp());
        String city = IpUtils.stringSplit(Objects.requireNonNull(IpUtils.getIp2region(IpUtils.getIp(request))))[1].replace("省", "");
        user.setAscription(city);
        user.setIp(IpUtils.getIp(request));
        int num = userMapper.updateUserTimeIp(user);
        ResultSet<Integer> resultSet = new ResultSet<>();
        resultSet.setData(num);
        return resultSet;
    }

}
