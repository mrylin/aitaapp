package com.aitaapp.service;

import com.aitaapp.entity.LikeFootprints;
import com.aitaapp.mapper.LikeFootprintsMapper;
import com.aitaapp.service.impl.LikeFootprintsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class LikeFootprintsService implements LikeFootprintsImpl {
    public LikeFootprintsMapper likeFootprintsMapper;

    @Autowired
    public LikeFootprintsService(LikeFootprintsMapper likeFootprintsMapper) {
        this.likeFootprintsMapper = likeFootprintsMapper;
    }

}
