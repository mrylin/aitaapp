package com.aitaapp.service;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Report;
import com.aitaapp.mapper.ReportMapper;
import com.aitaapp.mapper.UserMapper;
import com.aitaapp.service.impl.ReportImpl;
import com.aitaapp.service.impl.UserImpl;
import com.aitaapp.utils.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportService implements ReportImpl {
    public ReportMapper reportMapper;

    @Autowired
    public ReportService(ReportMapper reportMapper) {
        this.reportMapper = reportMapper;
    }

    @Override
    public ResultSet<Number> report(Report report) {
        Report report1 = reportMapper.getReport(report.getReportId(), report.getReportedId());
        ResultSet<Number> resultSet = new ResultSet<>();
        if (report1 == null) {
            report.setCreationTime(DateTime.getTimestamp());
            reportMapper.addReport(report);
        } else {
            resultSet.setData(report1.getPenaltyStatus());
        }
        return resultSet;
    }
}
