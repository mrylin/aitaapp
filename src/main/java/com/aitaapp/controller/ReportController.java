package com.aitaapp.controller;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Report;
import com.aitaapp.service.ReportService;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/report")
public class ReportController {

    public ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping("/report")
    public ResultSet<Number> report(@RequestBody Report report) {
       return reportService.report(report);
    }
}
