package com.aitaapp.controller;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Visitor;
import com.aitaapp.service.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/visitor")
public class VisitorController {
    public VisitorService visitorService;

    @Autowired
    VisitorController(VisitorService visitorService) {
        this.visitorService = visitorService;
    }

    @PostMapping("/addVisitor")
    public ResultSet<String> addVisitor(@RequestBody Visitor visitor) {
        return visitorService.addVisitor(visitor);
    }
}
