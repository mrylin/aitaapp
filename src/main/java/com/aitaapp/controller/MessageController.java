package com.aitaapp.controller;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Message;
import com.aitaapp.service.MessageService;
import com.aitaapp.websocket.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/message")
public class MessageController {
    public WebSocket webSocket;
    public MessageService messageService;

    @Autowired
    public void webSocket(WebSocket webSocket, MessageService messageService) {
        this.webSocket = webSocket;
        this.messageService = messageService;
    }

    @PostMapping("/sendMsg")
    public ResultSet sendMsg(Message message) {

        return new ResultSet<>();
    }

    @GetMapping("/getMsgDetailed")
    public ResultSet<List<Message>> getMsgDetailed(@RequestParam("pageIndex") int pageIndex,
                                                   @RequestParam("pageSize") int pageSize, Message message) {
        return messageService.getMsgDetailed(pageIndex, pageSize, message.getSend(), message.getReceive());
    }

    @GetMapping("/getAllMsg")
    public ResultSet<List<Message>> getAllMsg(Message message) {
        return messageService.getAllMsg(message.getId());
    }

    @PostMapping("/updateById")
    public ResultSet updateById(@RequestBody Message message) {
        return messageService.updateById(message.getId());
    }

}
