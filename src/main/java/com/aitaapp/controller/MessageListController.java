package com.aitaapp.controller;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.bean.SystemMsg;
import com.aitaapp.entity.MessageList;
import com.aitaapp.entity.User;
import com.aitaapp.service.MessageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/messageList")
public class MessageListController {
    public MessageListService messageListService;

    @Autowired
    public MessageListController(MessageListService messageListService) {
        this.messageListService = messageListService;
    }

    @GetMapping("/getMsgList")
    public ResultSet<List<User>> getMsgList(User user) {
        return messageListService.getMsgList(user.getId());
    }

    @GetMapping("/getSystemMsg")
    public ResultSet<SystemMsg> getSystemMsg(MessageList messageList) {
        return messageListService.getSystemMsg(messageList.getId());
    }

    @PostMapping("/delMsgList")
    public ResultSet delMsgList(@RequestBody MessageList messageList) {
        return messageListService.delMsgList(messageList.getSend(),messageList.getReceive());
    }

    @PostMapping("/updateMsgList")
    public ResultSet updateMsgList(@RequestBody MessageList messageList) {
        return messageListService.updateMsgList(messageList.getSend(),messageList.getReceive());
    }

    @PostMapping("/updateSystemMsg")
    public ResultSet updateSystemMsg(@RequestBody MessageList messageList) {
        return messageListService.updateSystemMsg(messageList.getReceive());
    }
}
