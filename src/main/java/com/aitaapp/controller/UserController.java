package com.aitaapp.controller;

import com.aitaapp.bean.Like;
import com.aitaapp.bean.PaginationResult;
import com.aitaapp.bean.Login;
import com.aitaapp.bean.ResultSet;
import com.aitaapp.bean.request.ReqUserLogin;
import com.aitaapp.bean.request.ReqUserRecommend;
import com.aitaapp.entity.LikeFootprints;
import com.aitaapp.entity.User;
import com.aitaapp.service.UserService;
import com.aitaapp.websocket.QiNiuUpload;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    public UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public Login login(@RequestBody ReqUserLogin reqLogin, HttpServletRequest request) {
        return userService.login(reqLogin.getPhone(), reqLogin.getCode(), request);
    }

    @GetMapping("/getRecommend")
    public PaginationResult<User> getRecommend(@RequestParam("pageIndex") int pageIndex,
                                               @RequestParam("pageSize") int pageSize,
                                               @RequestParam(value = "latest", required = false, defaultValue = "false") Boolean latest,
                                               User user) {
        return userService.getRecommend(pageIndex, pageSize, latest, user);
    }

    @PostMapping("IlikeIt")
    public ResultSet<Long> IlikeIt(@RequestBody LikeFootprints likeFootprints) {
        return userService.IlikeIt(likeFootprints);
    }

    @GetMapping("/getSiftRecommend")
    public PaginationResult<User> getSiftRecommend(@RequestParam("pageIndex") int pageIndex,
                                                   @RequestParam("pageSize") int pageSize,
                                                   @RequestParam(value = "latest", required = false) Boolean latest,
                                                   ReqUserRecommend reqUserRecommend) {
        return userService.getSiftRecommend(pageIndex, pageSize, latest, reqUserRecommend);
    }

    @GetMapping("/getUserId")
    public ResultSet<User> getUserId(@RequestParam("receive") Long receive, @RequestParam(value = "id",required = false) Long id) {
        return userService.getUserId(receive, id);
    }

    @GetMapping("/getCountLike")
    public ResultSet<Like> getCountLike(@RequestParam("id") Long id) {
        return userService.getCountLike(id);
    }

    @GetMapping("/getLikeUser")
    public ResultSet<List<User>> getLikeUser(@RequestParam("pageIndex") int pageIndex,
                                             @RequestParam("pageSize") int pageSize,
                                             @RequestParam("id") Long id,
                                             @RequestParam("type") Long type) {
        return userService.getLikeUser(pageIndex, pageSize, id, type);
    }

    @GetMapping("/getCurrentUser")
    public ResultSet<User> getCurrentUser(@RequestParam("id") Long id) {
        return userService.getCurrentUser(id);
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("files") MultipartFile file) {
        QiNiuUpload qiNiuUpload = new QiNiuUpload();
        return qiNiuUpload.Upload(file);
    }

    @PostMapping("/delFile")
    public int delFile(@RequestBody User user) {
        QiNiuUpload qiNiuUpload = new QiNiuUpload();
        return qiNiuUpload.delFile(user.getHeadSculpture());
    }

    @PostMapping("/delFileMultiple")
    public ResultSet delFileMultiple(@RequestBody User user) {
        QiNiuUpload qiNiuUpload = new QiNiuUpload();
        return qiNiuUpload.delBatchFile(user.getPhotos());
    }

    @PostMapping("/updateUser")
    public ResultSet<Integer> updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @PostMapping("/updateUserTimeIp")
    public ResultSet<Integer> updateUserTimeIp(@RequestBody User user, HttpServletRequest request) {
        return userService.updateUserTimeIp(user,request);
    }
}
