package com.aitaapp;

import com.aitaapp.websocket.WebSocket;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

//扫描mapper接口
@MapperScan(basePackages = "com.aitaapp.mapper")
@SpringBootApplication
public class AitaappApplication {

    public static void main(String[] args) {
        //修改后-- //解决websocketServer无法注入mapper问题
        SpringApplication springApplication = new SpringApplication(AitaappApplication.class);
        ConfigurableApplicationContext configurableApplicationContext = springApplication.run(args);
        WebSocket.setApplicationContext(configurableApplicationContext);
    }

}
