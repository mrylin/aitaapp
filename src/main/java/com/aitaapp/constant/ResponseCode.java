package com.aitaapp.constant;

public class  ResponseCode {
    public static final Long code200 = 200L;
    public static final Long code403 = 403L;
    public static final Long code500 = 500L;
}
