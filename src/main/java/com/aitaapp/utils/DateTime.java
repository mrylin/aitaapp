package com.aitaapp.utils;

import java.util.Date;

public class DateTime {


    public static Long getTimestamp() {
        Date date = new Date();
        return date.getTime();
    }

    public static Long getExpirationTime() {
        //30天过期
        return DateTime.getTimestamp() + (30L * 1000 * 60 * 60 * 24);
    }
    public static Long getExpirationTime7() {
        //7天过期
        return DateTime.getTimestamp() + (7L * 1000 * 60 * 60 * 24);
    }
}
