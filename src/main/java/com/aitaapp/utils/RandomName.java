package com.aitaapp.utils;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class RandomName {
    private static final int length = 10;

    public static String getRandomJianHan() {
        String randomName = "";
        for (int i = 0; i < length; i++) {
            String str = null;
            int hightPos, lowPos; // 定义高低位
            Random random = new Random();
            hightPos = (176 + Math.abs(random.nextInt(39))); // 获取高位值
            lowPos = (161 + Math.abs(random.nextInt(93))); // 获取低位值
            byte[] b = new byte[2];
            b[0] = (Integer.valueOf(hightPos).byteValue());
            b[1] = (Integer.valueOf(lowPos).byteValue());
            try {
                str = new String(b, "GBK"); // 转成中文
            } catch (UnsupportedEncodingException ex) {
                ex.printStackTrace();
            }
            randomName += str;
        }
        return randomName;
    }

    public static void main(String[] args) {
        System.out.println(getRandomJianHan());
    }
}
