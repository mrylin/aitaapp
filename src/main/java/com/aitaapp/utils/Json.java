package com.aitaapp.utils;

import com.aitaapp.bean.ResultSet;
import com.aitaapp.entity.Message;
import com.aitaapp.entity.User;
import com.aitaapp.websocket.MessageObj;
import com.aitaapp.websocket.SocketResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class Json {
    public static MessageObj obj(String json) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, MessageObj.class);
    }

    public static String string(ResultSet<Message> obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
}
