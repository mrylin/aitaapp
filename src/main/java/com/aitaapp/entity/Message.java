package com.aitaapp.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class Message {
    private Long id;
    private Long send;
    private Long receive;
    private String message;
    private Long msgTime;
    private Long expirationTime;
    private Long readMsg;
    private Long systemMsg;
    private String url; //头像
    private Long currentUser;
}
