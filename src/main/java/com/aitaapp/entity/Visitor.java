package com.aitaapp.entity;

import lombok.Data;

@Data
public class Visitor {
    private Long id;
    private Long haveYouSeenMe;
    private Long pid;
    private Long accessTime;
}
