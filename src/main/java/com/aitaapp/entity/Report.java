package com.aitaapp.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class Report {
    private Long id;
    private String type;
    private String msgIds;
    private String message;
    private Long reportId;
    private Long reportedId;
    private Long penaltyStatus;
    private Long creationTime;
}
