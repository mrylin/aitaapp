package com.aitaapp.entity;

import lombok.Data;

import java.util.List;


@Data
public class User {
    public Long id;
    private String headSculpture;
    private String registrationType;
    private Long age;
    private Long height;
    private String marriage;
    private Long weight;
    private String education;
    private String address;
    private String selfIntroduction;
    private String gender;
    private String career;
    private Long income;
    private String child;
    private Long lastActiveTime;
    private Long creationTime;
    private String ip;
    private String punishmentType;
    private Long endTime;
    private Long mobilePhone;
    private String ascription;
    private String nickName;
    private Integer like; //是否喜欢
    private Long year; //哪一年出生的
    private Long unReadCount; //未读的条数
    private Long msgTime; //消息最新时间
    private List<LifePhotos> LifePhotos; //生活照对象
    private List<String> photos; //生活照链接
}
