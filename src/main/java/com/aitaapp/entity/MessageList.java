package com.aitaapp.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class MessageList {
    private Long id;
    private Long send;
    private Long receive;
    private Long currentUser;
}
