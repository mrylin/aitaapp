package com.aitaapp.entity;

import lombok.Data;

@Data
public class LikeFootprints {
    private Long id;
    private Long likeMe;
    private Long likeIt;
    private Long pid;
}
