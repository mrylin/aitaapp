package com.aitaapp.entity;

import lombok.Data;

import java.time.Instant;

@Data
public class LifePhotos {
    private Long id;
    private Long pid;
    private String url;
}
