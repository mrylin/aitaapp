package com.aitaapp.mapper;


import com.aitaapp.entity.LifePhotos;
import com.aitaapp.entity.LikeFootprints;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;
import java.util.List;

public interface LifePhotosMapper {
    @Select("select * from lifephotos where pid = #{id}")
    List<LifePhotos> getListPhotos(@Param("id") Long id);

    @Delete("delete from lifephotos where pid = #{pid}")
    Long delPhotos(@Param("pid") Long pid);

    @Insert("insert into lifephotos (url,pid) values (#{url},#{pid})")
    Long addPhotos(@Param("url") String url,@Param("pid") Long pid);
}
