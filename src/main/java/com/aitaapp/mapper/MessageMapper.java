package com.aitaapp.mapper;


import com.aitaapp.entity.Message;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface MessageMapper {
    @Select("select count(*) from message as m where m.currentUser = #{receive} and m.send = #{send} and m.receive = #{receive} and m.readMsg = #{readMsg}")
    Long getMsgCount(@Param("send") Long send, @Param("receive") Long receive, @Param("readMsg") Long readMsg);

    @Select("select * from message as m where m.currentUser = #{receive} and (m.send = #{send} or m.receive = #{send}) order by id DESC")
    List<Message> getMsgDetailed(@Param("send") Long send, @Param("receive") Long receive);

    @Update("update message as m set m.readMsg = 1 where m.currentUser = #{receive} and m.send = #{send} and m.receive = #{receive} and m.readMsg = 0")
    Integer UpdateRead(@Param("send") Long send, @Param("receive") Long receive);

    @Select("select * from message as m where  m.currentUser = #{receive} and (m.send = #{send} or m.receive = #{send}) order by m.msgTime desc limit 1")
    Message getNewMsgTime(@Param("send") Long send, @Param("receive") Long receive);

    @Select("select * from message where receive = #{id} and  systemMsg = 1 order by msgTime DESC limit 1")
    Message getSystemMsg(@Param("id") Long id);

    @Select("select count(*) from message as m where m.receive = #{id} and m.readMsg = 0 and  m.systemMsg = 1")
    Long getSystemMsgCount(@Param("id") Long id);

    @Select("update message as m set m.readMsg = 1 where m.receive = #{id} and m.systemMsg = 1")
    Integer updateSystemMsg(@Param("id") Long id);

    int addMessage(@Param("msg") Message message);

    @Select("select * from message where id = #{id}")
    Message getById(@Param("id") Long id);

    @Select("select * from message where receive = #{id} and systemMsg = 1 order by msgTime DESC")
    List<Message> getAllMsg(@Param("id") Long id);

    @Select("update message set readMsg = 1 where id = #{id} and systemMsg = 1")
    Integer updateById(@Param("id") Long id);
}
