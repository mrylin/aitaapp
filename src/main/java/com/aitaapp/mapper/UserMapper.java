package com.aitaapp.mapper;


import com.aitaapp.bean.request.ReqUserRecommend;
import com.aitaapp.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper {
    @Select("select * from user where mobilePhone=#{phone}")
    User login(@Param("phone") Long phone);

    int insertUser(@Param("creationTime") Long creationTime,
                   @Param("mobilePhone") Long mobilePhone,
                   @Param("ip") String ip,
                   @Param("ascription") String ascription,
                   @Param("nickName") String nickName);

    List<User> getRecommend(@Param("user") User user);

    List<User> getSiftRecommend(@Param("latest") Boolean latest, @Param("recommend") ReqUserRecommend recommend);

    List<User> getForEach(@Param("list") List<Long> list);

    @Select("select * from  user where id = #{id}")
    User getUserId(@Param("id") Long id);

    int updateUser(User user);

    @Update("update user set lastActiveTime = #{lastActiveTime},ip = #{ip},ascription = #{ascription} where id = #{id}")
    int updateUserTimeIp(User user);

    @Update("update user set punishmentType = #{punishmentType},endTime = #{endTime} where id = #{id}")
    int updatePunishmentType(@Param("punishmentType") String punishmentType, @Param("endTime") Long endTime, @Param("id") Long id);

}
