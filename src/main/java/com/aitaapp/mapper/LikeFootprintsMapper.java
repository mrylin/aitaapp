package com.aitaapp.mapper;


import com.aitaapp.entity.LikeFootprints;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;
import java.util.List;

public interface LikeFootprintsMapper {

    @Select("select id,likeMe,likeIt,pid from likefootprints where pid = #{id}")
    ArrayList<LikeFootprints> getLikeIt(@Param("id") Long id);

    Long addIlikeIt(LikeFootprints likeFootprints);

    @Insert("insert into likefootprints (likeMe,pid) values (#{pid},#{likeMe})")
    Long addIikeMe(@Param("pid") Long pid, @Param("likeMe") Long likeMe);

    @Select("select id,likeMe,likeIt,pid from likefootprints where pid = #{id} and likeIt=#{likeID}")
    LikeFootprints getIlikeIt(@Param("id") Long id, @Param("likeID") Long likeID);

    @Delete("delete from likefootprints  where pid = #{id} and likeIt=#{likeID}")
    Long delLike(@Param("id") Long id, @Param("likeID") Long likeID);

    @Delete("delete from likefootprints  where pid = #{id} and likeMe=#{likeMe}")
    Long delIikeMe(@Param("id") Long id, @Param("likeMe") Long likeMe);

    @Select("select * from likefootprints where likeIt = #{receive} and pid = #{id}")
    LikeFootprints getUserLikeIt(@Param("receive") Long receive, @Param("id") Long id);

    @Select("select count(*) from likefootprints where pid = #{id} and likeIt IS NOT NULL")
    Long countILike(@Param("id") Long id);

    @Select("select count(*) from likefootprints where  pid = #{id} and likeMe IS NOT NULL")
    Long countLikeMe(@Param("id") Long id);

    @Select("select * from likefootprints where pid = #{id} and likeIt IS NOT NULL")
    List<LikeFootprints> getILike(@Param("id") Long id);

    @Select("select * from likefootprints where pid = #{id} and likeMe IS NOT NULL")
    List<LikeFootprints> getLikeMe(@Param("id") Long id);

}
