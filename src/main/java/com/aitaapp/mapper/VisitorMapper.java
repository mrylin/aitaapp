package com.aitaapp.mapper;

import com.aitaapp.entity.Visitor;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface VisitorMapper {
    @Insert("insert into visitor (pid,haveYouSeenMe,accessTime) values (#{pid},#{haveYouSeenMe},#{accessTime})")
    int addVisitor(@Param("pid") Long pid, @Param("haveYouSeenMe") Long haveYouSeenMe, @Param("accessTime") Long accessTime);

    @Select("select * from visitor where pid = #{pid} and haveYouSeenMe = #{haveYouSeenMe}")
    Visitor getVisitor(@Param("pid") Long pid, @Param("haveYouSeenMe") Long haveYouSeenMe);

    @Select("select count(*) from visitor where pid= #{id}")
    Long countSeenMe(@Param("id") Long id);

    @Select("select * from visitor where pid= #{id}")
    List<Visitor> getSeenMe(@Param("id") Long id);
}

