package com.aitaapp.mapper;


import com.aitaapp.entity.Report;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ReportMapper {
    @Select("select * from report where reportId = #{reportId} and reportedId = #{reportedId} and penaltyStatus = 0")
    Report getReport(@Param("reportId") Long reportId, @Param("reportedId") Long reportedId);

    @Insert("insert into report (type,msgIds,message,reportId,reportedId,creationTime,penaltyStatus) values (#{type},#{msgIds},#{message},#{reportId},#{reportedId},#{creationTime},0)")
    int addReport(Report report);
}
