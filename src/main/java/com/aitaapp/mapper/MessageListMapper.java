package com.aitaapp.mapper;


import com.aitaapp.entity.MessageList;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MessageListMapper {
    @Select("select send,receive from messagelist where currentUser = #{id}")
    List<MessageList> getMsgList(@Param("id") Long id);

    @Delete("delete from messagelist where currentUser = #{send} and (send = #{receive} or receive = #{receive})")
    int delMsgList(@Param("send") Long send, @Param("receive") Long receive);

    @Select("select * from messagelist where currentUser = #{currentUser} and (send = #{receive} or receive = #{receive})")
    MessageList getMessageList(@Param("currentUser") Long currentUser, @Param("receive") Long receive);

    @Insert("insert into messagelist set send = #{send}, receive = #{receive}, currentUser = #{currentUser}")
    int addMessageList(@Param("send") Long send,@Param("currentUser") Long currentUser, @Param("receive") Long receive);
}
