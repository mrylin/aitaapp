package com.aitaapp.bean;

import lombok.Data;

@Data
public class SystemMsg {
    private Long unReadCount;
    private String message;
    private Long msgTime;
}
