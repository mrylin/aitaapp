package com.aitaapp.bean;

import com.aitaapp.entity.User;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Login {
    private String token;
    private Integer code = 200;
    private String msg = "成功";
    private User data;
}
