package com.aitaapp.bean;

import com.aitaapp.constant.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PaginationResult<T> {
    private Long code = ResponseCode.code200;
    private String msg = "成功";
    private List<T> data;
    private int pageIndex; //当前页
    private Long total; //总条数
}
