package com.aitaapp.bean;

import com.aitaapp.constant.ResponseCode;
import lombok.Data;
import org.springframework.stereotype.Component;


@Data
@Component
public class ResultSet<T> {
    private Long code = ResponseCode.code200;
    private String msg = "成功";
    private T data;
}
