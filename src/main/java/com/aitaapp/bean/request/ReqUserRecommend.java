package com.aitaapp.bean.request;

import lombok.Data;

import java.lang.reflect.Array;
import java.util.ArrayList;

@Data
public class ReqUserRecommend {
    public Long id;
    private String headSculpture;
    private String registrationType;
    private ArrayList<Long> age;
    private ArrayList<Long> height;
    private String marriage;
    private ArrayList<Long> weight;
    private String education;
    private String address;
    private String selfIntroduction;
    private String gender;
    private String career;
    private ArrayList<Long> income;
    private String child;
    private Long lastActiveTime;
    private Long creationTime;
    private String ip;
    private String punishmentType;
    private Long endTime;
    private Long mobilePhone;
    private String ascription;
    private String nickName;
    private Integer like;
    private Long year;
}
