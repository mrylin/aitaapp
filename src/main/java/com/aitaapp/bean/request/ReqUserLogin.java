package com.aitaapp.bean.request;

import lombok.Data;

@Data
public class ReqUserLogin {
    private Long phone;
    private String code;
}
