package com.aitaapp.bean;

import lombok.Data;


@Data
public class Like {
    private Long ILike; //我喜欢的
    private Long LikeMine; //喜欢我的
    private Long SeenMe; //看过我
}
